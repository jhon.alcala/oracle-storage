FROM node:boron

MAINTAINER Reekoh

COPY . /home/node/oracle-storage

WORKDIR /home/node/oracle-storage

RUN apt-get update && apt-get install -y build-essential \
	apt-get install alien libaio1 libaio-dev -y \
	wget "https://s3.amazonaws.com/reekoh-rpms/oracle-instantclient12.1-basic-12.1.0.2.0-1.x86_64.rpm" -O oracle-instantclient12.1-basic-12.1.0.2.0-1.x86_64.rpm \
	wget "https://s3.amazonaws.com/reekoh-rpms/oracle-instantclient12.1-devel-12.1.0.2.0-1.x86_64.rpm" -O oracle-instantclient12.1-devel-12.1.0.2.0-1.x86_64.rpm \
  alien -i oracle-instantclient12.1-basic-12.1.0.2.0-1.x86_64.rpm \
  alien -i oracle-instantclient12.1-devel-12.1.0.2.0-1.x86_64.rpm \
	npm install --production

RUN npm install pm2@2.6.1 -g

CMD pm2-docker --json app.yml